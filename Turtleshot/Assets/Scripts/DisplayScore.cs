﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    private GameObject gameManager;
    private ScoreManager scoreManager;
    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager");
        scoreManager = gameManager.GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = scoreManager.score.ToString();
    }
}
