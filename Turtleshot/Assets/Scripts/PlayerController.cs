﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private enum LR {
        Null,
        L,
        R
    }
    float chargeL = 0;
    float chargeR = 0;
    bool triggerPrevL = false;
    bool triggerPrevR = false;
    float cooldownL = 0;
    float cooldownR = 0;
    public GameObject bulletSpawnLocationL;
    public GameObject bulletSpawnLocationR;
    public GameObject bulletPrefab;
    private BulletScript bulletScript;
    public Rigidbody rb;
    //public Rigidbody cloneRb;
    [SerializeField] public float moveSpeed;
    [SerializeField] public float bulletSpeed;

    public void Awake () {
        bulletScript = GetComponent<BulletScript> ();
        rb = GetComponent<Rigidbody> ();
    }

    void Update () {
        float vertical = Input.GetAxis ("Vertical");
        Vector3 pos;
        pos = transform.position;
        pos.y = transform.position.y + (4f * vertical * Time.deltaTime) + (5 * Time.deltaTime);
        transform.position = pos;

        float horizontal = Input.GetAxis ("Horizontal");
        if (horizontal > 0.5f) {
            MoveRight ();
            MoveRight ();
        } else if (horizontal < -0.5f) {
            MoveLeft ();
            MoveLeft ();
        }
        ShootingLogic ();
    }

    void MoveRight () {
        transform.position += transform.right * moveSpeed * Time.deltaTime;
    }

    void MoveLeft () {
        transform.position -= transform.right * moveSpeed * Time.deltaTime;
    }

    void ShootingLogic () {
        float triggerL = Input.GetAxis ("FireL");
        float triggerR = Input.GetAxis ("FireR");
        if (triggerL > 0.5f) {
            triggerPrevL = true;
            chargeL += 1 * Time.deltaTime;
        } else if (triggerL < 0.5f && triggerPrevL) {
            triggerPrevL = false;
            Shoot (chargeL, LR.L);
            chargeL = 0;
        }
        if (triggerR > 0.5f) {
            triggerPrevR = true;
            chargeR += 1 * Time.deltaTime;
        } else if (triggerR < 0.5f && triggerPrevR) {
            triggerPrevR = false;
            Shoot (chargeR, LR.R);
            chargeR = 0;
        }
    }
    void Shoot (float aCharge, LR aSide) {
        GameObject clone;
        Rigidbody bulletRb;
        if (aCharge <= 0) {
            return;
        }
        switch (aSide) {
            case LR.L:
                if (cooldownL < Time.time) {
                    cooldownL = Time.time + 0.5f;
                } else {
                    return;
                }
                break;
            case LR.R:
                if (cooldownR < Time.time) {
                    cooldownR = Time.time + 0.5f;
                } else {
                    return;
                }
                break;
        }

        clone = Instantiate (bulletPrefab, transform.position, Quaternion.identity);
        bulletRb = clone.GetComponent<Rigidbody> ();
        clone.GetComponent<BulletScript> ().bulletSize = aCharge;
        switch (aSide) {
            case LR.L:
                clone.transform.position = bulletSpawnLocationL.transform.position;
                bulletRb.velocity = new Vector3 (-10, 10, 0);

                break;
            case LR.R:
                clone.transform.position = bulletSpawnLocationR.transform.position;
                bulletRb.velocity = new Vector3 (10, 10, 0);
                break;
        }
        Destroy (clone, 5.0f);
    }

    void OnTriggerEnter (Collider collision) {
        if (collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "Enemy") {
            UnityEngine.SceneManagement.SceneManager.LoadScene ("GameOver");
        }
    }

}