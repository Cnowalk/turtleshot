﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float bulletSize = 0.1f;
    private ScoreManager scoreManager;
    private GameObject gameManager;

    [SerializeField] EnemyType type = EnemyType.Sea;

    // Start is called before the first frame update
    void Start()
    {
        checkSize();
        gameManager = GameObject.FindWithTag("GameManager");
        scoreManager = gameManager.GetComponent<ScoreManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bullet")
        {
            BulletScript otherBullet = other.GetComponent<BulletScript>();
            if (bulletSize < otherBullet.bulletSize) //Destroy this bullet
            {
                Destroy(gameObject);
                //--otherBullet.bulletSize;
                //otherBullet.checkSize();
            }
            else if (bulletSize > otherBullet.bulletSize) //Destroy other bullet
            {
                //Destroy(collision.gameObject);
                --bulletSize;
                checkSize();
            }
            else if(bulletSize == otherBullet.bulletSize) //Destroy both bullets
            {
                Destroy(gameObject);
            }
        }
        else if(/*other.tag == "Player" ||*/ other.tag == "Enemy")
        {
            scoreManager.score++;
            //Do nothing for now
        }
        else
        {
            if(type == EnemyType.Sea)
            Destroy(gameObject);
        }
    }

    void checkSize()
    {
        if(bulletSize <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            Vector3 size = transform.localScale;
            size.x = bulletSize;
            size.y = bulletSize;
            size.z = bulletSize;
            transform.localScale = size;
            GetComponent<SphereCollider>().radius = 0.5f * bulletSize;
        }
    }

    private enum EnemyType
    {
        Sea,
        Land
    }
}
