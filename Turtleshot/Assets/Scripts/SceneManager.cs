﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public static SceneManager instance;
    [HideInInspector] public int level = 1;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    private void Update() 
    {
        if(Input.GetButtonDown("Submit"))
        {
            loadLevel();
        }  
    }
    public void loadLevel()
    {
        switch (level)
        {
            case 1:
                UnityEngine.SceneManagement.SceneManager.LoadScene("Level1");
                break;

        }
    }

    public void GameOver()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game Over");
    }
}
