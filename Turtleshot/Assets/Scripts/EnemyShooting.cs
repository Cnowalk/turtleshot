﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float shootingSpeed = 1;
    [SerializeField] private int bulletsPerShot = 1;
    [SerializeField] private float deviation = 0; //0 is shooting straight, 90 is max spread
    [SerializeField] private float bulletSpeed = 5;

    private float shotTimer = 0;

    // Update is called once per frame
    void Update()
    {
        //Interval at which to shoot
        if (shotTimer <= 0)
        {
            //Shoot
            for (int i = 0; i < bulletsPerShot; ++i)
            {
                //Calculate deviation
                int posOrNeg = Random.Range(0, 2) * 2 - 1;
                float deviate = Random.Range(1, deviation) * posOrNeg;
                Vector3 rotDeviation = new Vector3(0, 0, deviate);

                //Intantiate the shot
                GameObject shot = Instantiate(bullet, transform);
                Destroy(shot, 5);
                shot.transform.eulerAngles += rotDeviation;
                shot.transform.position = transform.position + (shot.transform.up * -1);
                shot.transform.SetParent(null);
                shot.GetComponent<Rigidbody>().velocity = shot.transform.up * bulletSpeed * -1;
            }

            //Reset timer
            shotTimer = shootingSpeed;
        }
        else
        {
            shotTimer -= Time.deltaTime;
        }
    }
}
