﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreUpdate : MonoBehaviour
{
    [SerializeField] private Text scoreDisplay;
    private GameObject gameManager;
    private ScoreManager scoreManager;
    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager");
        scoreManager = gameManager.GetComponent<ScoreManager>();
        scoreManager.score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreDisplay.text = scoreManager.score.ToString();
    }
}
