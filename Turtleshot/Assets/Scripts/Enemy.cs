﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject player;

    private Vector3 playerPos;

    // Update is called once per frame
    void Update()
    {
        //Get player position
        getPlayerPos(player.transform.position);

        //Rotates enemy toward player
        transform.LookAt(playerPos);
    }

    private void getPlayerPos(Vector3 pos) //Method for assigning player position
    {
        playerPos = pos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bullet")
        {
            Destroy(gameObject);
        }
    }
}
